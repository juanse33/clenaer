<?php
require 'persistencia/aseadorDAO.php';
class Aseador extends Persona{
    private $estado;
    private $conexion;
    private $idempresa;
    private $foto;
    private $aseadorDAO;
    
    function Aseador($id="", $nombre="", $apellido="", $correo="", $clave="", $estado="",$idempresa="", $idlocalidad="", $foto=""){
        $this -> Persona($id, $nombre, $apellido, $correo, $clave); 
        $this -> estado = $estado;    
        $this -> idempresa = $idempresa;
        $this -> idlocalidad = $idlocalidad;
        $this -> foto = $foto;
        $this -> conexion = new Conexion();
        $this -> aseadorDAO = new AseadorDAO($id, $nombre, $apellido, $correo, $clave, $estado,$idempresa,$idlocalidad,$foto );
    }

    function autenticar(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> aseadorDAO -> autenticar());
        if($this -> conexion -> numFilas() == 1 ){
            $registro = $this -> conexion -> extraer();                        
            $this -> id = $registro[0];
            $this -> nombre = $registro[1];
            $this -> apellido = $registro[2];
            $this -> correo = $registro[3];
            $this -> estado = $registro[5];
            $this -> idempresa = $registro[6];
            $this -> idlocalidad = $registro[7];
            $this -> conexion -> cerrar();
            return true;
        }else{
            
            $this -> conexion -> cerrar();
            return false;
        }
    }   
    
    function existeCorreo(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> aseadorDAO -> existeCorreo());
        if($this -> conexion -> numFilas() == 1){
            $this -> conexion -> cerrar();
            return true;
        }else{
            $this -> conexion -> cerrar();
            return false;
        }
    }

    function insertar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> aseadorDAO -> insertar());
        
        $this -> conexion -> cerrar();
    }

    function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> aseadorDAO -> consultar());

        $registro = $this -> conexion -> extraer();
        $this -> id = $registro[0];
        $this -> nombre = $registro[1];
        $this -> apellido = $registro[2];
        $this -> correo = $registro[3];
        $this -> estado = $registro[5];
        $empresa = new Empresa($registro[6]);
        $empresa -> consultar();
        $this -> idempresa = $empresa;
        $this -> conexion -> cerrar();
    }

    function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar ($this -> aseadorDAO -> consultarTodos());
        $registros = array();
        for($i = 0; $i < $this -> conexion -> numFilas(); $i++){
            $registro = $this -> conexion -> extraer();
            $registros[$i] = new aseador ($registro[0], $registro[1], $registro[2], $registro[3], "", $registro [5],$registro[6]);
        }
        return $registros;
    }

    function consultarLocalidad(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar ($this -> aseadorDAO -> consultarLocalidad());
        $registros = array();
        for($i = 0; $i < $this -> conexion -> numFilas(); $i++){
            $registro = $this -> conexion -> extraer();
            $empresa = new Empresa($registro[6]);
            $empresa -> consultar();
            $localidad = new Localidad($registro[7]);
            $localidad -> consultar();
            $registros[$i] = new aseador ($registro[0], $registro[1], $registro[2], $registro[3], "", $registro [5],$empresa, $localidad, $registro[8]);
        }
        return $registros;
    }

    function aseadorDisponible($fecha, $horario){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> aseadorDAO -> aseadorDisponible($fecha, $horario));
        $registros = array();
        for($i = 0; $i < $this -> conexion -> numFilas(); $i++){
          $registro = $this -> conexion -> extraer();
          $empresa = new Empresa($registro[6]);
          $empresa -> consultar();
          $localidad = new Localidad($registro[7]);
          $localidad -> consultar();
          $registros[$i] = new aseador ($registro[0], $registro[1], $registro[2], $registro[3], "", $registro [5],$empresa, $localidad, $registro[8]);
      }
        $this -> conexion -> cerrar();
        return $registros;
      }

    function cambiarEstado() {
        $this -> conexion -> abrir();
        $this -> conexion ->  ejecutar($this -> aseadorDAO -> cambiarEstado());
        $this -> conexion -> cerrar();
    }

    function actualizar(){
        $this -> conexion -> abrir();
        $this -> conexion ->  ejecutar($this-> aseadorDAO -> actualizar());
        $datos = array();
        for($i = 0; $i < $this -> conexion -> numFilas(); $i++){
            $dato = $this -> conexion -> extraer();
            $datos[$i] = new aseador ($dato[0], $dato[1], $dato[2], $dato[3], "", $dato [4],"",$dato[5]);
        }
        return $datos;
    }
    function buscar($filtro){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> aseadorDAO -> buscar($filtro));
        $registros = array();
        for($i = 0; $i < $this -> conexion -> numFilas(); $i++){
            $registro = $this -> conexion -> extraer();           
            $registros[$i] = new aseador($registro[0], $registro[1], $registro[2], $registro[3],"",$registro[4],"",$registro[5]);
        }
        return $registros;
    }
    function consultarEmpresa($filtro){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> aseadorDAO -> consultarEmpresa($filtro));
        $registros = array();
        for($i = 0; $i < $this -> conexion -> numFilas(); $i++){
            $registro = $this -> conexion -> extraer();           
            $registros[$i] = new aseador($registro[0], $registro[1], $registro[2], $registro[3],"",$registro[4]);
        }
        return $registros;
    } 
    function consultarClave(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> aseadorDAO -> consultarClave());
        $registro = $this -> conexion -> extraer();
        $this -> clave = $registro[0];
        $this -> conexion -> cerrar();
        return $this -> clave;
    }
    function actualizarClave($clave){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> aseadorDAO -> actualizarClave($clave));
        $this -> conexion -> cerrar();
    }
    function getClave(){
        return $this -> clave;
    }
    function getEstado(){
        return $this -> estado;
    }

    function getFoto(){
        return $this -> foto;
    }

    function getEmpresa(){
        return $this -> idempresa;
    }
    
    function getLocalidad(){
        return $this -> idlocalidad;
    }
    

}
?>