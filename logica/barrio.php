<?php
include 'persistencia/barrioDAO.php';
include 'logica/localidad.php';

class Barrio{
private $idbarrio;
private $idlocalidad; 
private $nombre;
private $conexion;
private $barrioDAO;

function barrio ($idbarrio="",$idlocalidad="",$nombre=""){
    $this -> idbarrio = $idbarrio;
    $this -> idlocalidad = $idlocalidad; 
    $this -> nombre = $nombre;
    $this -> conexion = new Conexion();
    $this -> barrioDAO = new BarrioDAO($idbarrio,$idlocalidad,$nombre);
}
function consultar(){
    $this -> conexion -> abrir();
    $this -> conexion -> ejecutar($this -> barrioDAO -> consultar());
    $registro = $this -> conexion -> extraer();
    $this -> nombre = $registro[0];   
    $this -> conexion -> cerrar();
}
function consultarTodos(){
    $this -> conexion -> abrir();
    $this -> conexion -> ejecutar($this -> barrioDAO -> consultarTodos());
    $registros = array();
    for($i = 0; $i < $this -> conexion -> numFilas(); $i++){
        $registro = $this -> conexion -> extraer();
        $f = new localidad($registro[1]);
        $f -> consultar();            
        $registros[$i] = new Barrio($registro[0],$f, $registro[2]);
    }
    return $registros;
}
function buscar($filtro){
    $this -> conexion -> abrir();
    $this -> conexion -> ejecutar($this -> barrioDAO -> buscar($filtro));
    $registros = array();
    for($i = 0; $i < $this -> conexion -> numFilas(); $i++){
        $registro = $this -> conexion -> extraer();           
        $registros[$i] = new Barrio($registro[0], $registro[1], $registro[2]);
    }
    return $registros;
} 
function getId(){
    return $this -> idbarrio;
}

function getNombre(){
    return $this->nombre;
}
function getIdlocalidad(){
    return $this -> idlocalidad;
}
}
 ?>