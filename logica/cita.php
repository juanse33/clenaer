<?php
require 'persistencia/citaDAO.php';

class Cita{
  private $idcita;
  private $fecha_cita;
  private $idtipo;
  private $idhorario;
  private $idaseador;
  private $idusuario;
  private $citaDAO;
  private $conexion;

  function Cita($idcita="", $fecha_cita="",$idtipo="",$idhorario="",$idaseador="", $idusuario=""){
    $this -> idcita = $idcita;
    $this -> fecha_cita = $fecha_cita;
    $this -> idtipo = $idtipo	;
    $this -> idhorario = $idhorario;
    $this -> idaseador = $idaseador;
    $this -> idusuario = $idusuario;
    $this -> conexion = new Conexion();
    $this -> citaDAO = new  CitaDAO($idcita, $fecha_cita, $idtipo, $idhorario, $idaseador, $idusuario);
  }
  function insertar(){
    $this -> conexion -> abrir();
    $this -> conexion -> ejecutar($this -> citaDAO -> insertar());
    $this -> conexion -> cerrar();
}

function consultarUsuarios(){
    $this -> conexion -> abrir();
    $this -> conexion -> ejecutar($this -> reservaDAO -> consultarUsuarios());
    $registros = array();
    for($i = 0; $i < $this -> conexion -> numFilas(); $i++){
      $registro = $this -> conexion -> extraer();
      $hotel = new Hotel($registro[3]);
      $hotel -> consultar();
      $registros[$i] = new Reserva($registro[0], $registro[1], $registro[2], $hotel , $registro[4]);
    }
    $this -> conexion -> cerrar();
    return $registros;
  }



function consultarTodos(){
    $this -> conexion -> abrir();
    $this -> conexion -> ejecutar ($this -> reservaDAO -> consultarTodos());
    $registros = array();
    for($i = 0; $i < $this -> conexion -> numFilas(); $i++){
        $registro = $this -> conexion -> extraer();
        $registros[$i] = new Reserva($registro[0], $registro[1], $registro[2], $registro[3]);
    }
    return $registros;
}

  function getFecha_entrada(){
    return $this -> fecha_entrada;
  }
  function getFecha_salida(){
    return $this -> fecha_salida;
  }
  function getCantidad_personas(){
    return $this -> cantidad_personas	;
  }
  function getHotel_idhotel(){
    return $this -> hotel_idhotel;
  }
  function getUsuario_idusuario(){
    return $this -> usuario_idusuario;
  }
}