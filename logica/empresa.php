<?php
require 'persistencia/empresaDAO.php';
class Empresa{
    private $idempresa;
    private $nombre ;
    private $correo ;
    private $direccion ;
    private $telefono;
    private $clave;
    private $conexion;
    private $empresaDAO;

    function Empresa($idempresa="",$nombre="",$correo="",$direccion="",$telefono="",$clave=""){
        $this -> conexion = new Conexion();
        $this -> idempresa = $idempresa;
        $this -> nombre = $nombre;
        $this -> correo = $correo;
        $this -> direccion = $direccion;
        $this -> telefono = $telefono;
        $this -> clave= $clave;
        $this -> empresaDAO = new EmpresaDAO($idempresa, $nombre, $correo, $direccion,$telefono, $clave );
    }
    
    function insertar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> empresaDAO -> insertar());
        $this -> conexion -> cerrar();
    }
    function autenticar(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> empresaDAO -> autenticar());
        if($this -> conexion -> numFilas() == 1){
            $registro = $this -> conexion -> extraer();                        
            $this -> idempresa = $registro[0];
            $this -> conexion -> cerrar();
            return true;
        }else{
            $this -> conexion -> cerrar();
            return false;
        }
    }
    
    function existeCorreo(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> empresaDAO -> existeCorreo());
        if($this -> conexion -> numFilas() == 1){
            $this -> conexion -> cerrar();
            return true;
        }else{
            $this -> conexion -> cerrar();
            return false;
        }
    }
    function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> empresaDAO -> consultar());
        $registro = $this -> conexion -> extraer();
        $this -> idempresa = $registro[0];
        $this -> nombre = $registro[1];
        $this -> correo = $registro[2];
        $this -> direccion = $registro[3];
        $this -> telefono = $registro[4];
        $this -> conexion -> cerrar();
    }
    function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar ($this -> empresaDAO -> consultarTodos());
        $registros = array();
        for($i = 0; $i < $this -> conexion -> numFilas(); $i++){
            $registro = $this -> conexion -> extraer();
            $registros[$i] = new Empresa($registro[0], $registro[1], $registro[2],$registro[3],$registro[4]);
        }
        $this -> conexion ->cerrar();
        return $registros;
    }
    function editarPerfiL(){
        $this -> conexion -> abrir();
        $this -> conexion ->  ejecutar($this-> empresaDAO -> editarPerfil());
        $this -> conexion -> cerrar();
    }   
    function buscar($filtro){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> empresaDAO -> buscar($filtro));
        $registros = array();
        for($i = 0; $i < $this -> conexion -> numFilas(); $i++){
            $registro = $this -> conexion -> extraer();           
            $registros[$i] = new Empresa($registro[0], $registro[1], $registro[2],$registro[3],$registro[4]);
        }
        return $registros;
    } 
    
    function getId(){
        return $this -> idempresa;
    }
    function getNombre(){
        return $this -> nombre;
    }
    function getCorreo(){
        return $this -> correo;
    }

    function getDireccion(){
        return $this -> direccion;
    }
    function gettelefono(){
        return $this -> telefono;
    }

    
}