<?php
    require 'persistencia/horarioDAO.php';
    
    class Horario{
        private $idhorario;
        private $nombre;
        private $conexion;
        private $horarioDAO;

        function Horario ($idhorario="",$nombre=""){
            $this -> idhorario = $idhorario;
            $this -> nombre = $nombre;
            echo $this -> idhorario;
            echo $this -> nombre;
            $this -> conexion = new Conexion();
            $this -> horarioDAO = new HorarioDAO($idhorario,$nombre);
        }
      
        function consultarTodos(){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> horarioDAO -> consultarTodos());
            echo $this -> horarioDAO -> consultarTodos();
            $registros = array();
            for($i = 0; $i < $this -> conexion -> numFilas(); $i++){
                $registro = $this -> conexion -> extraer();         
                $registros[$i] = new Horario($registro[0],$registro[1]);
            }
            return $registros;
        }

        function consultar(){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> horarioDAO -> consultar());
            echo $this -> horarioDAO -> consultar();
            $registro = $this -> conexion -> extraer();
            $this -> idhorario = $registro[0];
            $this -> nombre = $registro[1];       
            $this -> conexion -> cerrar();
        }

        function getId(){
            return $this -> idhorario;
        }

        function getNombre(){
            
            return $this -> nombre;
        }
    }
?>