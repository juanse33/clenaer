<?php
require 'persistencia/localidadDAO.php';
class localidad{
    private $id;
    private $nombre;
    private $conexion;
    private $localidadDAO;
    
    function localidad($id="", $nombre=""){
        $this -> id = $id;
        $this -> nombre = $nombre;
        $this -> conexion = new Conexion();
        $this -> localidadDAO = new localidadDAO($id, $nombre);
    }
    
    function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar ($this -> localidadDAO -> consultarTodos());
        $registros = array();
        for($i = 0; $i < $this -> conexion -> numFilas(); $i++){
            $registro = $this -> conexion -> extraer();
            $registros[$i] = new localidad($registro[0], $registro[1]);
        }
        $this -> conexion ->cerrar();
        return $registros;
    }
    function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> localidadDAO -> consultar());
        $registro = $this -> conexion -> extraer();
        $this -> id= $registro[0];
        $this -> nombre = $registro[1];   
        $this -> conexion -> cerrar();
    }
    
    function buscar($filtro){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> localidadDAO -> buscar($filtro));
        $registros = array();
        for($i = 0; $i < $this -> conexion -> numFilas(); $i++){
            $registro = $this -> conexion -> extraer();           
            $registros[$i] = new Localidad($registro[0], $registro[1]);
        }
        return $registros;
    }  
    
    function getId(){
        return $this -> id;
    }
    function getNombre(){
        return $this -> nombre;
    }

}
?>