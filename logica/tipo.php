<?php
    require 'persistencia/tipoDAO.php';
    class Tipo{
        private $idtipo;
        private $nombre;
        private $conexion;
        private $tipoDAO;

        function Tipo ($idtipo="",$nombre=""){
            $this -> idtipo = $idtipo;
            $this -> nombre = $nombre;
            $this -> conexion = new Conexion();
            $this -> tipoDAO = new TipoDAO($idtipo,$nombre);
        }

        function consultarTodos(){
            $this -> conexion -> abrir();
            $this -> conexion -> ejecutar($this -> tipoDAO -> consultarTodos());
            $registros = array();
            for($i = 0; $i < $this -> conexion -> numFilas(); $i++){
                $registro = $this -> conexion -> extraer();         
                $registros[$i] = new Tipo($registro[0],$registro[1]);
            }
            return $registros;
        }

        function getId(){
            return $this -> idtipo;
        }

        function getNombre(){
            return $this -> nombre;
        }
    }
?>