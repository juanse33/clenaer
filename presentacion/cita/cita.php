<?php
include 'presentacion/menuus.php';

if(isset($_POST['buscar'])){
    header("Location:index.php?pid=" . base64_encode("presentacion/cita/hacerCita.php") . "&idLocalidad=" . $_POST["idLocalidad"] . "&fecha=" . $_POST["fecha"] . "&horario=" . $_POST["horario"] . "&tipo=" . $_POST["tipo"]);
}
?>
<br/>
<br/>

<div class="container">

	<div class="row">
		<div class="col-1"></div>
		<div class="col-10">
			<div class="card ">
            	<div class="card-body">
					<form method="post">
                    <div class="row">
						<div class="col">
						<label>Localidad</label>
						<input type="text" class="form-control rounded-pill" placeholder="Ingresa tu localidad"
						name="localidad" id="localidad">
						<input name="idLocalidad" id="idLocalidad" type="hidden" class="form-control input-lg ">
						<div id="resultados"></div>
						</div>
						<div class="col">
						<label>Fecha en la que desea el servicio</label><input type="date" class="form-control rounded-pill" name="fecha">
						</div>
						<div class="col">
						<label>Elige el horario</label>
						<select class="custom-select" name="horario">
								<?php
                                
									$horario = new Horario();
									$horarios = $horario -> consultarTodos();
									
									foreach($horarios as $t){
											echo "<option value=". $t -> getId() .">". $t -> getNombre() ."</option>";
										
									}  								
								?>
						</select>
						</div>
                        <div class="col">
						<label>Elige tipo de localicion</label>
						<select class="custom-select" name="tipo">
								<?php
                                
									$tipo = new Tipo();
									$tipos = $tipo -> consultarTodos();
									echo "<option>Seleccione una opcion</option>";
									foreach($tipos as $t){
											echo "<option value=". $t -> getId() .">". $t -> getNombre() ."</option>";
										
									}  								
								?>
						</select>
						</div>
						<div class="col">
                        <label></label><br/>
                        <button type="submit" name="buscar" id="buscar" class="btn btn-primary">Buscar</button>
						</div>
					</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<br/>

<script type="text/javascript">
$(document).ready(function(){
	$("#localidad").keyup(function(){
		if($("#localidad").val()!=""){
			var ruta = "indexAjax.php?pid=<?php echo base64_encode("presentacion/cita/localidadAjax.php"); ?>&localidad="+$("#localidad").val();
			$("#resultados").load(ruta);
		}
	});
 	$("#localidad").focusout(function(){
		var ruta = "indexAjax.php?pid=<?php echo base64_encode("presentacion/cita/localidadAjax.php"); ?>&localidad=-1";
 		$("#resultados").load();
 	});
	
});


</script>
  


  <br/>
<br/>
<div class="container">
  <!--div class="card text-white text-center " style="background-color: #4d0000;" ><h4>Destinos mas visitados</h4></div-->
	<div class="card-group">
  <div class="card">
    <div class="card-body">
      <h5 class="card-title">Bogota</h5>
      <img src="img/bogota.jpg" class="card-img-top" alt="..."> 
      <br/><br/><p class="card-text">Bogotá es la capital y la ciudad más grande de Colombia. Punto de convergencia de personas de todo el país, es diversa y multicultural y en ella se combinan construcciones modernas con otras que evocan su pasado colonial.</p>
    </div>
  </div>
  <div class="card">
  <div class="card-body">
      <h5 class="card-title">Cartagena</h5>
      <img src="img/cartagena.jpg" class="card-img-top" alt="..."> 
      <br/><br/><p class="card-text">Cartagena de Indias es el destino turístico más importante de Colombia y uno de los sitios turísticos más destacados y reconocidos en todo el mundo.</p>
    </div>
  </div>
  <div class="card">
  <div class="card-body">
      <h5 class="card-title">Santa Marta</h5>
      <img src="img/marta.jpg" class="card-img-top" alt="..."> 
      <br/><br/><p class="card-text">Santa Marta es sabrosa; es una ciudad donde la diversidad está presente en su territorio, en su gente y en cada aspecto cultural que la compone, es un destino que acoge a todos sus visitantes y los hace sentir como en casa.</p>
    </div>
  </div>
  <div class="card">
  <div class="card-body">
      <h5 class="card-title">Buenos Aires</h5>
      <img src="img/buenos.jpg" class="card-img-top" alt="..."> 
      <br/><br/><p class="card-text">Buenos Aires, capital de Argentina, es una gran urbe de carácter cosmopolita que se ha convertido en una de las ciudades más importantes de Latinoamérica. Buenos Aires es una ciudad moderna que ha sabido conservar sus antiguas tradiciones y algunos de sus entrañables rincones, componiendo un lugar capaz de sorprender y enamorar a sus visitantes.</p>
    </div>
  </div>
  </div>
</div>

