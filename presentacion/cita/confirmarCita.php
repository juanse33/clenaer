<?php
include 'presentacion/menuus.php';
$aseador = new Aseador($_GET['idAseador']);
$aseador -> consultar();
$horario = new Horario($_GET['horario']);
$horario -> consultar();
$fecha = $_GET["fecha"];
$horario = $_GET["horario"];
$tipo = $_GET["tipo"];

if(isset($_POST["confirmar"])){
	$cita = new Cita("",$_GET['fecha'],$_GET['tipo'],$_GET['horario'], $_GET["idAseador"],$_SESSION["id"]);
	$cita -> insertar(); 
	}
?>
<br/>
<div class="container">
	<div class="row">
		<div class="col-2"></div>
		<div class="col-8">
			<div class="card ">
				<div class="card-body">
				<?php 
					if(isset($_POST["confirmar"])){
						echo "<div class='alert alert-success alert-dismissible fade show'
						role='alert'>
						Cita registrada
						<button type='button' class='close' data-dismiss='alert'
						aria-label='Close'>
						<span aria-hidden='true'>&times;</span>
						</button>
						</div>";
					}
				?>
				<div class="card-header  text-white" style="background-color:#4F066C;">Crear venta</div><br/>
					<form method="post">
						<div class="form-group">
						<?php 
							echo "<div class='card'>";
							echo "<div class='card-tittle'>";
							echo "<h5 class='card-title'>Tu cita</h5>";
							echo "<div class='card-body'>";
							echo "<img src=" . $aseador -> getFoto() ." class='card-img' alt='...'>";
							echo "<h6> Nombre:      " . $aseador -> getNombre() . "</h6>";
							echo "<h6> Empresa:      " . $aseador -> getEmpresa() -> getNombre() . "</h6>";
							echo "<h6> Fecha:      " . $fecha . "</h6>";
							echo "<h6> Horario:      " . $horario -> getNombre() . "</h6>";
							echo "<form method='post'>";
							echo "<button type='submit' name='confirmar' id='buscar' class='btn btn-primary'>Confirmar</button>";
							echo "</form>";
							echo "</div>";
							echo "</div>";
							echo "</div>";
						?>
						</div>

					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<br/>
<br/>

<script type="text/javascript">
$(document).ready(function(){
	$("#cliente").keyup(function(){
		if($("#cliente").val()!=""){
			var ruta = "indexAjax.php?pid=<?php echo base64_encode("presentacion/cliente/buscarClienteAjax.php"); ?>&cliente="+$("#cliente").val();
			$("#resultados").load(ruta)
		}
	});
 	$("#cliente").focusout(function(){
		var ruta = "indexAjax.php?pid=<?php echo base64_encode("presentacion/cliente/buscarClienteAjax.php"); ?>&cliente=-1";
 		$("#resultados").load();
 	});

});
</script>