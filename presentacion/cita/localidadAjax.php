<?php
$filtro = $_GET['localidad'];
if($filtro != "-1"){
    $localidad = new Localidad();
    $localidades = $localidad -> buscar($filtro);
    if(count($localidades)>0){
        echo "<div  class='list-group'>";
        foreach ($localidades as $c){
            echo "<button type='button' id='c". $c->getId() ."' class='list-group-item list-group-item-action'> ". $c -> getNombre(). "</button>";
        }
        echo "</div>";
    }
}else{
    echo "";
}
?>

<script type="text/javascript">
$(document).ready(function(){
	<?php 
	   foreach ($localidades as $c){
	       echo "$(\"#c" . $c -> getId() . "\").click(function(){\n";
	       echo "$(\"#localidad\").val(\"" . $c -> getNombre() . "\");\n";	       
	       echo "$(\"#idLocalidad\").val(\"" . $c-> getId() . "\");\n";
	       echo "});\n";
	   }
	   //Aqui se oculta la lista.
	   foreach ($localidades as $c){
	       echo "$(\"#c" . $c -> getId() . "\").click(function(){\n";
	       echo "$(\"#resultados\").hide();\n";	       
	       echo "});\n";
	   }
	   
	?>
});
	
</script>