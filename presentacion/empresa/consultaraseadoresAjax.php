<?php
$filtro = $_GET['filtro'];
$aseador = new Aseador("","","","","","",$_GET['idempresa']);
$registros = $aseador->consultarempresa($filtro);
if(count($registros)>0){
	?>
<div class="container">
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-header bg-info text-white">Consultar aseador</div>
					<div class="card-body">
					<table class="table table-striped table-hover">
						<thead>
							<tr>
								<th scope="col">Nombre</th>
                                <th scope="col">Correo</th>
                                <th scope="col">Estado</th>
							</tr>
						</thead>
						<tbody>
						<?php 
							foreach ($registros as $a){
								
							    echo "<tr>";
							    echo "<td>" . $a -> getNombre() . " ". $a -> getApellido() . "</td>";
                                echo "<td>" . $a -> getCorreo() . "</td>";  
								echo "<td><div id='estado".$a->getId()."'>" . $a -> getEstado() . "</div></td>";
								echo "<td>";
								if($a -> getEstado() == 1){
									echo "<a id='cambiarEstado".$a->getId()."' href='#'>";
									echo "<i class='fas fa-user-times' data-toggle='tooltip' data-placement='left' title='Deshabilitar'></i>";
									echo "</a>";
								}else{
									echo "<a id='cambiarEstado".$a->getId()."' href='#'>";
									echo "<i class='fas fa-user-check' data-toggle='tooltip' data-placement='left' title='Habilitar'></i>";
									echo "</a>";
								}
								echo "</td>";
								
								echo "</tr>";
							}				
							echo "<tr><td colspan='6'><strong>" . count($registros) . " registros encontrados</strong></td></tr>";							
							?>
						</tbody>
					</table>
<?php } else { ?>
<div class="alert alert-danger alert-dismissible fade show"
	role="alert">
	No se encontraron resultados
	<button type="button" class="close" data-dismiss="alert"
		aria-label="Close">
		<span aria-hidden="true">&times;</span>
	</button>
</div>
<?php } ?>
<script>
$(document).ready(function(){
<?php 
foreach ($registros as $e) {
    echo "$(\"#cambiarEstado".$e->getId()."\").click(function(){\n";
    echo "var ruta = \"indexAjax.php?pid=" . base64_encode("presentacion/empresa/cambiarEstadoaseadorAjax.php") . "&idaseador=".$e->getId()."\";\n";
    echo "$(\"#estado".$e->getId()."\").load(ruta);\n";
    echo "});\n";
}
?>
});
</script>
				</div>
			</div>
		</div>
	</div>
</div>
