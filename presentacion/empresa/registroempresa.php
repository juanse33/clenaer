<?php
$error = 0;

if (isset($_POST['registrar'])) {
	$empresa = new empresa("", $_POST['nombre'], $_POST['correo'], $_POST['direccion'],$_POST['telefono'],$_POST['clave']  );
	$aseador = new aseador("","", "", "", $_POST['clave']);
	$administrador = new Administrador("", "", "", $_POST['correo']);
	$usuario = new Usuario("", "", "", $_POST['correo']);

	if (!$administrador->existeCorreo()) {

		if (!$usuario->existeCorreo()) {

				if (!$aseador->existeCorreo()) {

					$empresa->insertar();
					//header("Location:index.php?pid=" . base64_encode("presentacion/registroempresa.php"));
				} else {
					$error = 1;
				}
			
		} else {
			$error = 1;
		}
	} else {
		$error = 1;
	}
	}
	?>
	<style>
body {
	background-image:url("img/fondo.jpg");
	}
</style>
<br/>

	<div class="container">
		<div class="row">
			<div class="col-3"></div>
			<div class="col-6">
				<div class="card" style="background-color:rgba(0,0,0,0.5);">
				<div class="card-header text-white text-center" style="background-color: #02143B;">Registro</div>
					<div class="card-body">
						<?php if (isset($_POST['registrar'])) { ?>
							<div class="alert alert-<?php echo ($error == 0) ? "success" : "danger" ?> alert-dismissible fade show" role="alert">
								<?php echo ($error == 0) ? "Registro exitoso" : $_POST['correo'] . " ya existe"; ?>
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
						<?php } ?>
						<form method="post" auto-complete="off">
						<div class="form-group">
							<label class="col text-center text-white"><h6>Nombre</h6></label> <input name="nombre" type="text"
								class="form-control" placeholder="Digite Nombre"
								required="required">
						</div>
						<div class="form-group">
						<label class="col text-center text-white"><h6>Correo</h6></label>  <input name="correo" type="email"
								class="form-control" placeholder="Digite Correo"
								required="required">
						</div>
						<div class="form-group">
						<label class="col text-center text-white"><h6>Direccion</h6></label> <input name="direccion" type="text"
								class="form-control" placeholder="Digite Direccion"
								required="required">
						</div>
						<div class="form-group">
						<label class="col text-center text-white"><h6>Telefono</h6></label> <input name="telefono" type="text"
								class="form-control" placeholder="Digite Telefono"
								required="required">
						</div>
						<div class="form-group">
						<label class="col text-center text-white"><h6>Clave</h6></label> <input name="clave" type="password"
								class="form-control" placeholder="Clave" required="required">
						</div>
						<div class="row text-center">	
							<div class="col text-center">					
								<button type="submit" name="registrar" class="btn text-white " style="background-color: #02143B;" >Registrarse</button>
							</div>
							<div class="col text-center">					
							<?php echo "<a class='btn text-white' style='background-color: #021642;' href='index.php?pid=" . base64_encode("presentacion/login.php"). "'>Inicia sesion</a>"; ?>											
							</div>
						</div>)
					</form>
					</div>
				</div>
			</div>
		</div>
	</div>