<?php
$correo = "";
if (isset($_POST['correo'])) {
	$correo = $_POST['correo'];
}
$clave = "";
if (isset($_POST['clave'])) {
	$clave = $_POST['clave'];
}
$error = "";
if (isset($_POST['autenticar'])) {

	$administrador = new Administrador("", "", "", $correo, $clave);
	if ($administrador->autenticar()) {

		$_SESSION['id'] = $administrador->getId();
		$_SESSION['rol'] = "administrador";
		$pid = base64_encode("presentacion/sesionAdministrador.php");

		header('Location: index.php?pid=' . $pid);
	} else {
		$usuario = new Usuario("", "", "", $correo, $clave);
		if ($usuario->autenticar()) {
			$_SESSION['id'] = $usuario->getId();
			$_SESSION['rol'] = "usuario";
			$pid = base64_encode("presentacion/sesionUsuario.php");
			header('Location: index.php?pid=' . $pid);
		} else {
			$aseador = new Aseador("", "", "", $correo, $clave);
			if ($aseador->autenticar()) {
				$_SESSION['id'] = $aseador->getId();
				$_SESSION['rol'] = "aseador";
				$pid = base64_encode("presentacion/sesionAseador.php");
				header('Location: index.php?pid=' . $pid);
			} else {
				$empresa = new Empresa("", "", $correo,"","", $clave);
				if ($empresa->autenticar()) {
					$_SESSION['id'] = $empresa->getId();
					$_SESSION['rol'] = "empresa";
					$pid = base64_encode("presentacion/sesionEmpresa.php");
					header('Location: index.php?pid='. $pid );
				}
			}
		}
	}
}else{
	$error = 1;
}

?>
<br/>
<br/>
<br/>

<br/>	
<style>
body {
	background-color:rgba(250, 177, 160,0.5);
	}
</style>
<div class="container" >
	<div class="row">
		<div class="col-10">
			<div class="row">
				<div class="col-4">
				</div>
				<div class="col-6">
					<div class="card" style="background-color:rgba(0,0,0,0.5);">
					<?php 
						if ((isset($_POST['autenticar']))){
							if($error==1){
								echo "<div class='alert alert-danger alert-dismissible fade show'
								role='alert'>
								Error, usuario o contraseña incorrectas
								<button type='button' class='close' data-dismiss='alert'
								aria-label='Close'>
								<span aria-hidden='true'>&times;</span>
								</button>
								</div>";
							}
						}
					?>
						
						<div class="card-body">
							
							<form method="post">
								<div class="form-group">
									<label class="col text-center text-white"><h5>Correo</h5></label> <input  name="correo" type="email" class="form-control" placeholder="Digite Correo" required="required">
								</div>
								<div class="form-group">
									<label class="col text-center text-white"><h5>Clave</h5></label> <input name="clave" type="password" class="form-control" placeholder="Clave" required="required">
								</div>
								<div class="col text-center">
									<button type="submit" name="autenticar" class="btn text-white" style="background-color: #021642;">Iniciar Sesion</button>
								</div><br/><br/>
							    <div class="row text-center">
									<div class="col">
										<?php echo "<a class='btn text-white' style='background-color: #021642;' href='index.php?pid=" . base64_encode("presentacion/usuario/registrousuario.php"). "'><img src='img/hombre.png'>      Registrate</a>"; ?>											
										<?php echo "<a class='btn text-white' style='background-color: #021642;' href='index.php?pid=" . base64_encode("presentacion/empresa/registroempresa.php"). "'><img src='img/empresa.png'>      Registra tu empresa</a>"; ?>
									</div>
								</div>
							</form>						
					</div>
				</div>
			</div>
			<div class="col text-center">
				<br /><br />
			</div>



		</div>
	</div>
</div>
</div>
</div>
</div>