<?php
$error = 0;

if(isset($_POST['registrar'])){	
        $usuario = new Usuario("", $_POST['nombre'], $_POST['apellido'], $_POST['correo'], $_POST['clave'], "");
		$aseador = new aseador("","", "", "", $_POST['clave']);
		$administrador = new Administrador("", "", "", $_POST['correo']);
    if (!$administrador -> existeCorreo()){

			if(!$usuario -> existeCorreo()){
			
				if(!$aseador -> existeCorreo()){

					$usuario -> insertar();
					//header( "Location:index.php?pid=". base64_encode("presentacion/registro.php"));
				}else{
					$error = 1;
			}
			}else{
        $error = 1;
   	  }
    }else{
        $error = 1;
    }
}
?>
<style>
body {
	background-image:url("img/fondo.jpg");
	}
</style>
<br/>
<br/>
<div class="container text-white">
	<div class="row">
		<div class="col-3"></div>
		<div class="col-6">
			<div class="card " style="background-color:rgba(0,0,0,0.5);">
				<div class="card-header text-white text-center" style="background-color: #02143B;">Registro</div>
				<div class="card-body text-body">
					<?php if (isset($_POST['registrar'])) { ?>
					<div class="alert alert-<?php echo ($error==0) ? "success" : "danger" ?> alert-dismissible fade show"
						role="alert">
						<?php echo ($error==0) ? "Registro exitoso" : $_POST['correo'] . " ya existe"; ?>
						<button type="button" class="close" data-dismiss="alert"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<?php } ?>
					<form method="post" auto-complete="off">
						<div class="form-group">
							<label class="col text-center text-white"><h6>Nombre</h6></label> <input name="nombre" type="text"
								class="form-control" placeholder="Digite Nombre"
								required="required">
						</div>
						<div class="form-group">
						<label class="col text-center text-white"><h6>Apellido</h6></label>  <input name="apellido" type="text"
								class="form-control" placeholder="Digite Apellido"
								required="required">
						</div>
						<div class="form-group">
						<label class="col text-center text-white"><h6>Correo</h6></label> <input name="correo" type="email"
								class="form-control" placeholder="Digite Correo"
								required="required">
						</div>
						<div class="form-group">
						<label class="col text-center text-white"><h6>Clave</h6></label> <input name="clave" type="password"
								class="form-control" placeholder="Clave" required="required">
						</div>
						<div class="col text-center">					
							<button type="submit" name="registrar" class="btn text-white " style="background-color: #02143B;" >Registrarse</button>
						</div><br/>
						<div class="col text-center">					
						<?php echo "<a class='btn text-white' style='background-color: #021642;' href='index.php?pid=" . base64_encode("presentacion/login.php"). "'>Inicia sesion</a>"; ?>											
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>


