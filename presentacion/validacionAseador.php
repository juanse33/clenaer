<?php
if(isset($_SESSION['id'])){
    if($_SESSION['rol'] == "aseador"){
        $aseador = new aseador($_SESSION['id']);
        $aseador -> consultar();
        if($aseador -> getEstado() == 0) {
            header('Location: index.php');            
        }    
        
    }else{
        $pid = base64_encode("presentacion/error.php");
        header('Location: index.php?pid='. $pid);
    }
}else{
    header('Location: index.php');
}
?>