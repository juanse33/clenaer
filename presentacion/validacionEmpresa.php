<?php
if(isset($_SESSION['id'])){
    if($_SESSION['rol'] == "empresa"){
        $empresa = new Empresa($_SESSION['id']);
        $empresa -> consultar();
    }else{
        $pid = base64_encode("presentacion/error.php");
        header('Location: index.php?pid='. $pid);
    }
}else{
    header('Location: index.php');
}
?>